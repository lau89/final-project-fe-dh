import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRootComponent } from './components/home-root/home-root.component';
import { HomeNavComponent } from './components/home-nav/home-nav.component';
import { HomeUserComponent } from './components/home-user/home-user.component';
import {HomeRoutingModule} from './home-routing/home-routing.module';

@NgModule({
  declarations: [HomeRootComponent, HomeNavComponent, HomeUserComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class AppHomeModule { }
