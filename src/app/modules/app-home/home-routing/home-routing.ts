import {Routes} from '@angular/router';
import {HomeRootComponent} from "../components/home-root/home-root.component";
import {HomeUserComponent} from "../components/home-user/home-user.component";

export const HOME_ROUTES: Routes = [
  {
    path: '',
    component: HomeRootComponent,
    children: [
      {
        path: 'users',
        component: HomeUserComponent
      },
      // {
      //   path: 'resources',
      //   component: LobbyResourcesComponent
      // },
      {
        path: '',
        redirectTo: '/home/users',
        pathMatch: 'full'
      }
    ]
  }
];
