import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { PublicRootComponent } from './components/public-root/public-root.component';
import {PublicRoutingModule} from './public-routing/public-routing.module';

@NgModule({
  declarations: [LoginComponent, PublicRootComponent],
  imports: [
    CommonModule,
    PublicRoutingModule
  ]
})
export class AppPublicModule { }
