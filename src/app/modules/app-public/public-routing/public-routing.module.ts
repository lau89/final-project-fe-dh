import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {PUBLIC_ROUTES} from './public-routing';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(PUBLIC_ROUTES)
  ],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
