import {Routes} from '@angular/router';
import {PublicRootComponent} from '../components/public-root/public-root.component';
import {LoginComponent} from '../components/login/login.component';

export const PUBLIC_ROUTES: Routes = [
  {
    path: '',
    component: PublicRootComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'sign-up',
        component: LoginComponent
      },
      {
        path: '',
        redirectTo: '/secure/login',
        pathMatch: 'full'
      }
    ]
  }
];
