import {Routes} from '@angular/router';
import {NotFoundComponent} from '../components/not-found/not-found.component';

export const ROUTES_CONFIG: Routes = [
  {
    path: 'public',
    loadChildren: '../modules/app-public/app-public.module#AppPublicModule',
  },
  {
    path: 'home',
    loadChildren: '../modules/app-home/app-home.module#AppHomeModule'
  },
  {
    path: '',
    redirectTo: '/public/login',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
