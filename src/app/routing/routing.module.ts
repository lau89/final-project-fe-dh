import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ROUTES_CONFIG} from './app.routes';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(ROUTES_CONFIG)
  ],
  exports: [
    RouterModule
  ],
})
export class RoutingModule {
}
